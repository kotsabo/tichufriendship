//
//  Utils.swift
//  Tichu Friendship
//
//  Created by Ilias Kotsampougioukoglou on 04/06/2017.
//  Copyright © 2017 Ilias Kotsampougioukoglou. All rights reserved.
//

import Foundation
import UIKit

class Utils {
    
    //MARK: - Customize label
    
    static func resetLabel(label: UILabel) {
        label.layer.borderWidth = 1.5
        label.layer.borderColor = UIColor.darkGray.cgColor
    }
    
    static func showSelectionOfLabel(label: UILabel) {
        label.layer.borderColor = Color.Red.cgColor
        label.layer.borderWidth = 2
    }
    
    static func setupLabel(label: UILabel) {
        label.backgroundColor = UIColor.white
        label.layer.borderWidth = 1.5
        label.layer.borderColor = UIColor.darkGray.cgColor
        label.layer.cornerRadius = 2
        label.clipsToBounds = true
        label.textColor = UIColor.black
        label.text = ""
    }
    
    //MARK: - setup custom keyboard
    static func setupKeyboard(keyboardView: CustomKeyboardView, onView: UIView, onController: UIViewController) -> CustomKeyboardView {
        var tempKeyboardView = keyboardView
        tempKeyboardView = CustomKeyboardView.instanceFromNib() as! CustomKeyboardView
        tempKeyboardView.translatesAutoresizingMaskIntoConstraints = false
        onView.addSubview(tempKeyboardView)
        tempKeyboardView.setup()
        tempKeyboardView.keyboardDelegate = onController as? KeyboardDelegate
        return tempKeyboardView
    }
    
    static func setupConstraintsForKeyboard(keyboardView: CustomKeyboardView, onView: UIView) {
        let leadingConstraintOfFirstKeyboard = NSLayoutConstraint(item: keyboardView, attribute: .leading, relatedBy: .equal, toItem: onView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraintOfFirstKeyboard = NSLayoutConstraint(item: keyboardView, attribute: .trailing, relatedBy: .equal, toItem: onView, attribute: .trailing, multiplier: 1, constant: 0)
        let bottomConstraintOfFirstKeyboard = NSLayoutConstraint(item: keyboardView, attribute: .bottom, relatedBy: .equal, toItem: onView, attribute: .bottom, multiplier: 1, constant: 0)
        let heightOfFirstkeyboard = NSLayoutConstraint(item: keyboardView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 150)
        onView.addConstraints([leadingConstraintOfFirstKeyboard, trailingConstraintOfFirstKeyboard, bottomConstraintOfFirstKeyboard, heightOfFirstkeyboard])
    }
    
    //MARK: - customize tableview
    
    static func setupTable(withTableView: UITableView, withBackgroundColor: UIColor, onController: UIViewController) {
        withTableView.delegate = onController as? UITableViewDelegate
        withTableView.dataSource = onController as? UITableViewDataSource
        withTableView.tableFooterView = UIView()
        withTableView.backgroundColor = withBackgroundColor
    }
    
    //MARK: - setup Go Button
    
    static func setupGoButton(button: UIButton) {
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.backgroundColor = Color.LightBlue
        button.layer.cornerRadius = 2
        button.setTitleColor(UIColor.black, for: .normal)
    }
    
    //MARK: - show toast with message on specific view
    
    static func showToast(messsage: String, view: UIView) {
        let toastLabel = UILabel(frame: CGRect(x: view.frame.size.width/2 - 150, y: view.frame.size.height-100, width: 300,  height : 35))
        toastLabel.backgroundColor = UIColor.lightGray
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        view.addSubview(toastLabel)
        toastLabel.text = messsage
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.clipsToBounds  =  true
        
        UIView.animate(withDuration: 4.0, delay: 0.1, options: UIViewAnimationOptions.curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        })
        
    }
    
    //MARK: - calculate power of an integer on an integer
    
    static func power(base:Int, power:Int) -> Int {
        var answer : Int = 1
        for _ in 0..<power {
            answer *= base
        }
        return answer
    }
    
    //MARK: - take screenshot of view
    
    static func screenShotMethod(view: UIView, controller: UIViewController) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.shareImage(image: image!, controller: controller)
    }
    
    //MARK: - functionality for sharing
    
    static func shareImage(image: UIImage, controller: UIViewController) {
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [image as Any], applicationActivities: nil)

        let subject = "Tichu result"
        activityViewController.setValue(subject, forKey: "Subject")
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        activityViewController.excludedActivityTypes = [
            UIActivityType.postToWeibo,
            UIActivityType.print,
            UIActivityType.assignToContact,
            UIActivityType.saveToCameraRoll,
            UIActivityType.addToReadingList,
            UIActivityType.postToFlickr,
            UIActivityType.postToVimeo,
            UIActivityType.postToTencentWeibo
        ]
        
        controller.present(activityViewController, animated: true, completion: nil)
    }
    
    
}
