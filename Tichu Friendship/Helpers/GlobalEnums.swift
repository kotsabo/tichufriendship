//
//  GlobalEnums.swift
//  Tichu Friendship
//
//  Created by Ilias Kotsampougioukoglou on 19/06/2017.
//  Copyright © 2017 Ilias Kotsampougioukoglou. All rights reserved.
//

import Foundation

enum SpecificAction: Int {
    case clear = 11
    case tichuATeam = 12
    case minusTichuATeam = 13
    case tichuBTeam = 14
    case minusTichuBTeam = 15
}

enum TGame {
    case New_Game
    case Last_Game
    case Other
}

enum Options: String {
    case New_Game = "New game"
    case Current_Game = "Current game"
    case Previous_Games = "Previous games"
    case Undo_Last_Round = "Undo last round"
    case Share_Result = "Share result"
}
