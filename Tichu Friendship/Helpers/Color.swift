//
//  Color.swift
//  Tichu Friendship
//
//  Created by Ilias Kotsampougioukoglou on 15/06/2017.
//  Copyright © 2017 Ilias Kotsampougioukoglou. All rights reserved.
//

import Foundation
import UIKit

struct Color {
    

    static var AppleBlue: UIColor {
        get {
            return hexStringToUIColor(hex: "#1073FD")
        }
    }
    
    static var LightBlue: UIColor {
        get {
            return hexStringToUIColor(hex: "#B0E0E6")
        }
    }
    
    static var Red: UIColor {
        get {
            return hexStringToUIColor(hex: "#B21020")
        }
    }
    
    static var VeryLightBlue: UIColor {
        get {
            return hexStringToUIColor(hex: "#EFF8FA")
        }
    }
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        
        var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = cString.substring(from: cString.index(cString.startIndex, offsetBy: 1))
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
