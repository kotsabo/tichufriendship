//
//  DateHandler.swift
//  Tichu Friendship
//
//  Created by Ilias Kotsampougioukoglou on 14/06/2017.
//  Copyright © 2017 Ilias Kotsampougioukoglou. All rights reserved.
//

import Foundation

class DateHandler {
    
    static let dateFormatter: DateFormatter = DateFormatter()
    
    static func convertDateToString(date: Date) -> String {
        self.dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        let stringDate: String = dateFormatter.string(from: date)
        return stringDate
    }
    
}
