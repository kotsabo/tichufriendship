//
//  AllGamesTableViewCell.swift
//  Tichu Friendship
//
//  Created by Ilias Kotsampougioukoglou on 12/06/2017.
//  Copyright © 2017 Ilias Kotsampougioukoglou. All rights reserved.
//

import UIKit

class AllGamesTableViewCell: UITableViewCell {

    @IBOutlet weak var gameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var roundsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.black
        self.gameLabel.textColor = UIColor.white
        self.dateLabel.textColor = Color.VeryLightBlue
        self.roundsLabel.textColor = Color.VeryLightBlue
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
