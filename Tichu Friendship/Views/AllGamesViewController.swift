//
//  AllGamesViewController.swift
//  Tichu Friendship
//
//  Created by Ilias Kotsampougioukoglou on 12/06/2017.
//  Copyright © 2017 Ilias Kotsampougioukoglou. All rights reserved.
//

import UIKit

class AllGamesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var allGamesTableView: UITableView!
    
    var allGames: [Game] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.allGamesTableView.backgroundColor = UIColor.black
        self.allGamesTableView.delegate = self
        self.allGamesTableView.dataSource = self
        self.allGamesTableView.tableFooterView = UIView()
        if let tmpGames = CoreDataHandler.loadAllGames() {
            for game in tmpGames.reversed() {
                self.allGames.append(game)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func showMenu(_ sender: UIBarButtonItem) {
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }
    
    func constructGameLabel(index: Int) -> String {
        var totalGameString: String = ""
        let firstTeam = self.allGames[index].firstTeam.name
        if firstTeam != "" {
            totalGameString += firstTeam + ": "
        }
        else {
            totalGameString += "Team A" + ": "
        }
        totalGameString += String(self.allGames[index].firstTeam.totalScore) + "  vs  "
        let secondTeam = self.allGames[index].secondTeam.name
        if secondTeam != "" {
            totalGameString += secondTeam + ": "
        }
        else {
            totalGameString += "Team B" + ": "
        }
        totalGameString += String(self.allGames[index].secondTeam.totalScore)
        return totalGameString
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allGames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AllGamesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "gameCell", for: indexPath) as! AllGamesTableViewCell
        cell.gameLabel.text = self.constructGameLabel(index: indexPath.row)
        cell.dateLabel.text = "Last modified: " + DateHandler.convertDateToString(date: self.allGames[indexPath.row].modifiedDate)
        cell.roundsLabel.text = "Total rounds: \(self.allGames[indexPath.row].rounds)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "centerVC") as! ViewController
        centerViewController.state = TGame.Other
        if let tmpGame = CoreDataHandler.loadGame(id: self.allGames.count-1-indexPath.row) {
            centerViewController.game = tmpGame
            centerViewController.index = self.allGames.count-1-indexPath.row
        }
        let centerNavController = UINavigationController(rootViewController: centerViewController)
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.centerViewController = centerNavController
    }

}
