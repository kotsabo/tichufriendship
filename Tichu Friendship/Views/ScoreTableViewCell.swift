//
//  ScoreTableViewCell.swift
//  Tichu Friendship
//
//  Created by Ilias Kotsampougioukoglou on 03/06/2017.
//  Copyright © 2017 Ilias Kotsampougioukoglou. All rights reserved.
//

import UIKit

class ScoreTableViewCell: UITableViewCell {

    @IBOutlet weak var firstScoreLabel: UILabel!
    @IBOutlet weak var secondScoreLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setupCell() {
        self.backgroundColor = UIColor.black
        self.setupLabel(label: self.roundLabel, fontSize: 14)
        self.setupLabel(label: self.firstScoreLabel, fontSize: 15)
        self.setupLabel(label: self.secondScoreLabel, fontSize: 15)
    }
    
    private func setupLabel(label: UILabel, fontSize: CGFloat) {
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: fontSize)
    }

}
