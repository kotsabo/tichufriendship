//
//  ViewController.swift
//  Tichu Friendship
//
//  Created by Ilias Kotsampougioukoglou on 31/05/2017.
//  Copyright © 2017 Ilias Kotsampougioukoglou. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var teamALabel: UILabel!
    @IBOutlet weak var teamBLabel: UILabel!
    @IBOutlet weak var firstTotalScoreLabel: UILabel!
    @IBOutlet weak var secondTotalScoreLabel: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var goButton: UIButton!
    @IBOutlet weak var scoreTableView: UITableView!

    //MARK: - Properties
    
    private var keyboardView: CustomKeyboardView = CustomKeyboardView()
    var game: Game = Game()
    var state: TGame = TGame.Last_Game
    var index: Int = 0
    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Setup the views
    
    private func setup() {
        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.selectLabel))
        self.view.addGestureRecognizer(tapGesture)
        self.view.backgroundColor = Color.VeryLightBlue
        self.topView.backgroundColor = self.view.backgroundColor
        Utils.setupTable(withTableView: self.scoreTableView, withBackgroundColor: UIColor.black, onController: self)
        Utils.setupGoButton(button: self.goButton)
        Utils.setupLabel(label: self.firstLabel)
        Utils.setupLabel(label: self.secondLabel)
        self.keyboardView = Utils.setupKeyboard(keyboardView: self.keyboardView, onView: self.view, onController: self)
        Utils.setupConstraintsForKeyboard(keyboardView: self.keyboardView, onView: self.view)
        self.keyboardView.setID(ID: 1)
        Utils.showSelectionOfLabel(label: self.firstLabel)
    }
    
    @objc private func selectLabel(sender: UITapGestureRecognizer) {
        if self.firstLabel.frame.contains(sender.location(in: self.view)) {
            self.keyboardView.setID(ID: 1)
            Utils.resetLabel(label: self.secondLabel)
            Utils.showSelectionOfLabel(label: self.firstLabel)
        }
        else if self.secondLabel.frame.contains(sender.location(in: self.view)) {
            self.keyboardView.setID(ID: 2)
            Utils.resetLabel(label: self.firstLabel)
            Utils.showSelectionOfLabel(label: self.secondLabel)
        }
    }
    
    //MARK: - load data
    
    private func loadData() {
        let games = CoreDataHandler.loadAllGames()
        
        if self.state == TGame.Last_Game {
            guard games != nil else {return}
            if games!.isEmpty {
                self.index = 0
                CoreDataHandler.saveNewGame(game: self.game)
            }
            else {
                self.index = games!.count - 1
                self.game = games![index]
                self.firstTotalScoreLabel.text = String(self.game.firstTeam.totalScore)
                self.secondTotalScoreLabel.text = String(self.game.secondTeam.totalScore)
                self.scoreTableView.reloadData()
            }
        }
        else if self.state == TGame.New_Game {
            if games != nil {
                self.index = games!.count
            }
            else {
                self.index = 0
            }
            CoreDataHandler.saveNewGame(game: self.game)
        }
        else if self.state == TGame.Other {
            self.firstTotalScoreLabel.text = String(self.game.firstTeam.totalScore)
            self.secondTotalScoreLabel.text = String(self.game.secondTeam.totalScore)
        }
    }
    
    //MARK: - functionality for numbers
    
    private func checkIfInputsAreCorrect(firstInput: Int, secondInput: Int) -> (correction: Bool, message: String) {
        if (firstInput % 5 == 0 && secondInput % 5 == 0) && ((firstInput + secondInput) % DataSettingsController.TOTAL_SCORE == 0) {
            return (true,"correct")
        }
        return (false,"wrong input")
    }
    
    private func updateTotalForEachTeam() {
        self.firstTotalScoreLabel.text = String(self.game.firstTeam.totalScore)
        self.secondTotalScoreLabel.text = String(self.game.secondTeam.totalScore)
    }
    
    //MARK: - actions for buttons

    @IBAction func goButtonTapped(_ sender: UIButton) {
        if self.game.firstTeam.inputDigits.isEmpty || self.game.secondTeam.inputDigits.isEmpty {
            Utils.showToast(messsage: "empty fields", view: self.view)
        }
        else {
            let result = self.checkIfInputsAreCorrect(firstInput: self.game.firstTeam.inputNumber, secondInput: self.game.secondTeam.inputNumber)
            if result.correction {
                self.game.updateGame(scoreOfFirstTeam: self.game.firstTeam.inputNumber, scoreOfSecondTeam: self.game.secondTeam.inputNumber)
                self.updateTotalForEachTeam()
                self.scoreTableView.reloadData()
                self.scoreTableView.scrollToRow(at: IndexPath(row: self.game.rounds-1, section: 0), at: .bottom, animated: true)
                self.game.resetAllInputs()
                CoreDataHandler.updateGame(selectedGame: self.game, id: self.index)
                self.firstLabel.text = ""
                self.secondLabel.text = ""
            }
            else {
                Utils.showToast(messsage: result.message, view: self.view)
            }
        }
    }
    
    @IBAction func showMenu(_ sender: UIBarButtonItem) {
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }
    
    //MARK: - undo last round actions
    
    func actionsForUndoLastRound() {
        if self.game.rounds > 0 {
            self.showAlertForUndoLastRound()
        }
    }
    
    func showAlertForUndoLastRound() {
        let alertController = UIAlertController(title: "Undo last round", message: "Are you sure you want to undo last round?", preferredStyle: .actionSheet)
        let yesAction = UIAlertAction(title: "YES", style: .default, handler: self.undoLastRound)
        alertController.addAction(yesAction)
        let noAction = UIAlertAction(title: "NO", style: .default, handler: nil)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func undoLastRound(action: UIAlertAction) {
        self.game.deleteLastRoundOfGame()
        self.updateTotalForEachTeam()
        self.scoreTableView.reloadData()
        if self.game.rounds > 0 {
            self.scoreTableView.scrollToRow(at: IndexPath(row: self.game.rounds-1, section: 0), at: .bottom, animated: true)
        }
        CoreDataHandler.updateGame(selectedGame: self.game, id: self.index)
    }

    //MARK: - share actions
    
    func actionsForShare() {
        Utils.screenShotMethod(view: self.view, controller: self)
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - Delegate Methods for tableview
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.game.rounds
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ScoreTableViewCell = tableView.dequeueReusableCell(withIdentifier: "scoreCell", for: indexPath) as! ScoreTableViewCell
        cell.roundLabel.text = "\(indexPath.row+1)."
        cell.firstScoreLabel.text = "\(self.game.firstTeam.roundsScore[indexPath.row])"
        cell.secondScoreLabel.text = "\(self.game.secondTeam.roundsScore[indexPath.row])"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension ViewController: KeyboardDelegate {
    
    //MARK: - delegate method for keyboard
    
    func doAction(action: Int, view: CustomKeyboardView) {
        print("delegate \(action)")
        self.game.algorithmForActions(action: action, selection: view.getID())
        self.firstLabel.text = self.game.firstTeam.inputText
        self.secondLabel.text = self.game.secondTeam.inputText
    }
}


