//
//  CustomKeyboardView.swift
//  Tichu Friendship
//
//  Created by Ilias Kotsampougioukoglou on 31/05/2017.
//  Copyright © 2017 Ilias Kotsampougioukoglou. All rights reserved.
//

import UIKit

protocol KeyboardDelegate {
    func doAction(action: Int, view: CustomKeyboardView)
}

class CustomKeyboardView: UIView {

    var keyboardDelegate: KeyboardDelegate? = nil
    
    private var ID: Int = 0

    @IBOutlet weak var tichuATeamButton: UIButton!
    @IBOutlet weak var minusTichuATeamButton: UIButton!
    @IBOutlet weak var minusTichuBTeamButton: UIButton!
    @IBOutlet weak var tichuBTeamButton: UIButton!
    @IBOutlet weak var oneButton: UIButton!
    @IBOutlet weak var twoButton: UIButton!
    @IBOutlet weak var threeButton: UIButton!
    @IBOutlet weak var fourButton: UIButton!
    @IBOutlet weak var fiveButton: UIButton!
    @IBOutlet weak var sixButton: UIButton!
    @IBOutlet weak var sevenButton: UIButton!
    @IBOutlet weak var eightButton: UIButton!
    @IBOutlet weak var nineButton: UIButton!
    @IBOutlet weak var zeroButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup() {
        self.setupButtons()
    }
    
    private func setupButtons() {
        self.setupTichuButton(button: self.tichuATeamButton)
        self.setupTichuButton(button: self.minusTichuATeamButton)
        self.setupTichuButton(button: self.minusTichuBTeamButton)
        self.setupTichuButton(button: self.tichuBTeamButton)
        self.setupButton(button: self.oneButton)
        self.setupButton(button: self.twoButton)
        self.setupButton(button: self.threeButton)
        self.setupButton(button: self.fourButton)
        self.setupButton(button: self.fiveButton)
        self.setupButton(button: self.sixButton)
        self.setupButton(button: self.sevenButton)
        self.setupButton(button: self.eightButton)
        self.setupButton(button: self.nineButton)
        self.setupButton(button: self.zeroButton)
        self.setupButton(button: self.clearButton)
    }
    
    private func setupButton(button: UIButton) {
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        button.backgroundColor = UIColor.white
        button.setTitleColor(UIColor.black, for: .normal)
    }
    
    private func setupTichuButton(button: UIButton) {
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        button.backgroundColor = UIColor.lightGray
        button.setTitleColor(UIColor.black, for: .normal)
    }
    
    func setID(ID: Int) {
        self.ID = ID
    }
    
    func getID() -> Int {
        return self.ID
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CustomKeyboardView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        var action:Int = -1
        
        switch sender.tag {
        case 11:
            action = SpecificAction.clear.rawValue
        case 12:
            action = SpecificAction.tichuATeam.rawValue
        case 13:
            action = SpecificAction.minusTichuATeam.rawValue
        case 14:
            action = SpecificAction.tichuBTeam.rawValue
        case 15:
            action = SpecificAction.minusTichuBTeam.rawValue
        case 16:
            action = 0
        default:
            action = sender.tag
        }
        
        self.keyboardDelegate?.doAction(action: action, view: self)
    }

}
