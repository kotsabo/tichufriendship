//
//  MenuTableViewCell.swift
//  Tichu Friendship
//
//  Created by Ilias Kotsampougioukoglou on 04/06/2017.
//  Copyright © 2017 Ilias Kotsampougioukoglou. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var optionsLabel: UILabel!
    @IBOutlet weak var optionsImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = Color.VeryLightBlue
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
