//
//  MenuViewController.swift
//  Tichu Friendship
//
//  Created by Ilias Kotsampougioukoglou on 04/06/2017.
//  Copyright © 2017 Ilias Kotsampougioukoglou. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var optionsTableView: UITableView!
    @IBOutlet weak var titleView: UIView!
    
    //MARK: - Properties
    
    var optionsDataSource: [String] = []
    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Setup view
    
    private func setup() {
        self.view.backgroundColor = Color.VeryLightBlue
        self.titleView.backgroundColor = self.view.backgroundColor
        self.setupOptions()
        Utils.setupTable(withTableView: self.optionsTableView, withBackgroundColor: Color.VeryLightBlue, onController: self)
    }
    
    private func setupOptions() {
        self.optionsDataSource.append(Options.New_Game.rawValue)
        self.optionsDataSource.append(Options.Current_Game.rawValue)
        self.optionsDataSource.append(Options.Previous_Games.rawValue)
        self.optionsDataSource.append(Options.Undo_Last_Round.rawValue)
        self.optionsDataSource.append(Options.Share_Result.rawValue)
    }
    
    func createNewGame(action: UIAlertAction) {
//        let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "centerVC") as! ViewController
//        centerViewController.state = TGame.New_Game
//        let centerNavController = UINavigationController(rootViewController: centerViewController)
//        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.centerContainer!.centerViewController = centerNavController
//        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
        self.goToCenterViewController(withState: TGame.New_Game, completionHandler: nil)
    }
    
    func goToCenterViewController(withState: TGame, completionHandler: ((Bool)->())? ) {
        let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "centerVC") as! ViewController
        centerViewController.state = withState
        let centerNavController = UINavigationController(rootViewController: centerViewController)
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.centerViewController = centerNavController
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: completionHandler)
    }
    
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.optionsDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! MenuTableViewCell
        cell.optionsLabel.text = self.optionsDataSource[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch self.optionsDataSource[indexPath.row] {
        case Options.New_Game.rawValue:
            print("New game")
            let alertController = UIAlertController(title: "New game", message: "Are you sure you want to create a new game?", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "YES", style: .default, handler: self.createNewGame)
            alertController.addAction(yesAction)
            let noAction = UIAlertAction(title: "NO", style: .default, handler: nil)
            alertController.addAction(noAction)
            self.present(alertController, animated: true, completion: nil)
        case Options.Current_Game.rawValue:
            print("Current game")
            self.goToCenterViewController(withState: TGame.Last_Game, completionHandler: nil)
//            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "centerVC") as! ViewController
//            centerViewController.state = TGame.Last_Game
//            let centerNavController = UINavigationController(rootViewController: centerViewController)
//            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
//            appDelegate.centerContainer!.centerViewController = centerNavController
//            appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
        case Options.Previous_Games.rawValue:
            print("Previous games")
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "allGamesVC") as! AllGamesViewController
            let centerNavController = UINavigationController(rootViewController: centerViewController)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = centerNavController
            appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
        case Options.Undo_Last_Round.rawValue:
            print("Undo last round")
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            let navController: UINavigationController = appDelegate.centerContainer?.centerViewController as! UINavigationController
            if let controller = navController.viewControllers[0] as? ViewController {
                appDelegate.centerContainer!.toggle(.left, animated: true, completion: {(finish: Bool) in
                    controller.actionsForUndoLastRound()
                })
            }
            else {
                print("Current game")
                let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "centerVC") as! ViewController
                centerViewController.state = TGame.Last_Game
                let centerNavController = UINavigationController(rootViewController: centerViewController)
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = centerNavController
                appDelegate.centerContainer!.toggle(.left, animated: true, completion: {(finish: Bool) in
                    centerViewController.actionsForUndoLastRound()
                })
            }
        case Options.Share_Result.rawValue:
            print("Share your result")
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            let navController: UINavigationController = appDelegate.centerContainer?.centerViewController as! UINavigationController
            if let controller = navController.viewControllers[0] as? ViewController {
                appDelegate.centerContainer!.toggle(.left, animated: true, completion: {(finish: Bool) in
                    controller.actionsForShare()
                })
            }
            else {
                print("Current game")
                let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "centerVC") as! ViewController
                centerViewController.state = TGame.Last_Game
                let centerNavController = UINavigationController(rootViewController: centerViewController)
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = centerNavController
                appDelegate.centerContainer!.toggle(.left, animated: true, completion: {(finish: Bool) in
                    centerViewController.actionsForShare()
                })
            }
        default:
            print("wrong option")
        }
    }
    
}
