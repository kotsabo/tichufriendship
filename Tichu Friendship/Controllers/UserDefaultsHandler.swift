//
//  UserDefaultsHandler.swift
//  Tichu Friendship
//
//  Created by Ilias Kotsampougioukoglou on 11/06/2017.
//  Copyright © 2017 Ilias Kotsampougioukoglou. All rights reserved.
//

import Foundation

class UserDefaultsHandler {
    
    static func saveGame(game: Game) {
        //store you class object into NSUserDefaults.
        if let loadedData = UserDefaults.standard.value(forKey: "allGames") as? Data {
            if var gamesList = NSKeyedUnarchiver.unarchiveObject(with: loadedData) as? [Game] {
                gamesList.insert(game, at: 0)
                let gamesData = NSKeyedArchiver.archivedData(withRootObject: gamesList)
                UserDefaults().set(gamesData, forKey: "allGames")
            }
            else {
                print("wrong at unarchive data")
            }
        }
        else {
            print("no element at first time")
            var gamesList: [Game] = []
            gamesList.append(game)
            let gamesData = NSKeyedArchiver.archivedData(withRootObject: gamesList)
            UserDefaults().set(gamesData, forKey: "allGames")
        }
    }
    
    static func loadGames() -> [Game]? {
        //get your object from NSUserDefaults.
        if let loadedData = UserDefaults().data(forKey: "allGames") {
            if let gamesList = NSKeyedUnarchiver.unarchiveObject(with: loadedData) as? [Game] {
                return gamesList
            }
        }

        print("error at loading")
        return nil
    }
    
}
