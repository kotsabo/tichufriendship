//
//  CoreDataHandler.swift
//  Tichu Friendship
//
//  Created by Ilias Kotsampougioukoglou on 10/06/2017.
//  Copyright © 2017 Ilias Kotsampougioukoglou. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoreDataHandler {
    
    static func loadAllGames() -> [Game]? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TichuGame")
        do {
            if let fetchResults = try managedObjectContext.fetch(fetchRequest) as? [TichuGame] {
                var allGames: [Game] = []
                if !fetchResults.isEmpty {
                    for result in fetchResults {
                        if let game = result.game {
                            allGames.append(game)
                        }
                    }
                }
                return allGames
            }
        } catch let error as NSError {
            print(error)
        }
        
        return nil
    }
    
    static func loadGame(id: Int) -> Game? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TichuGame")
        do {
            if let fetchResults = try managedObjectContext.fetch(fetchRequest) as? [TichuGame] {
                print(fetchResults)
                if !fetchResults.isEmpty {
                    if let game = fetchResults[id].game {
                        return game
                    }
                }
            }
        } catch let error as NSError {
            print(error)
        }
        
        return nil
    }
    
    static func saveNewGame(game: Game) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let entityDescription = NSEntityDescription.entity(forEntityName: "TichuGame", in: managedObjectContext)
        let tichuGame = TichuGame(entity: entityDescription!, insertInto: managedObjectContext)
        tichuGame.game = game
        appDelegate.saveContext()
    }

    static func updateGame(selectedGame: Game, id: Int) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TichuGame")
        do {
            if let fetchResults = try managedObjectContext.fetch(fetchRequest) as? [TichuGame] {
                if !fetchResults.isEmpty {
                    fetchResults[id].game = selectedGame
                }
            }
        } catch let error as NSError {
            print(error)
        }
        
        appDelegate.saveContext()
    }
    
    static func deleteGame(selectedDame: Game, id: Int) {
        
    }
    
    static func deleteAllGames() {
        
    }
    
    
}
