//
//  Game.swift
//  Tichu Friendship
//
//  Created by Ilias Kotsampougioukoglou on 07/06/2017.
//  Copyright © 2017 Ilias Kotsampougioukoglou. All rights reserved.
//

import Foundation

public class Game: NSObject, NSCoding {
    
    var firstTeam: TichuTeam = TichuTeam()
    var secondTeam: TichuTeam = TichuTeam()
    var rounds: Int = 0
    var modifiedDate: Date = Date()
    
    override init() {
        
    }
    
    convenience required public init?(coder aDecoder: NSCoder) {
        self.init()
        self.firstTeam = aDecoder.decodeObject(forKey: "firstTeam") as! TichuTeam
        self.secondTeam = aDecoder.decodeObject(forKey: "secondTeam") as! TichuTeam
        self.rounds = aDecoder.decodeInteger(forKey: "rounds")
        self.modifiedDate = aDecoder.decodeObject(forKey: "modifiedDate") as! Date
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(firstTeam, forKey: "firstTeam")
        aCoder.encode(secondTeam, forKey: "secondTeam")
        aCoder.encode(rounds, forKey: "rounds")
        aCoder.encode(modifiedDate, forKey: "modifiedDate")
    }

    func updateGame(scoreOfFirstTeam: Int, scoreOfSecondTeam: Int) {
        self.rounds += 1
        self.firstTeam.updateScore(score: scoreOfFirstTeam)
        self.secondTeam.updateScore(score: scoreOfSecondTeam)
        self.setLastModifiedDate()
    }
    
    func deleteLastRoundOfGame() {
        self.rounds -= 1
        self.firstTeam.deleteLastRound()
        self.secondTeam.deleteLastRound()
        self.setLastModifiedDate()
    }
    
    func algorithmForActions(action: Int, selection: Int) {
        let currentTeam = self.inputForTeamWith(selection: selection)
        guard currentTeam != nil else {return}
        
        switch action {
        case SpecificAction.clear.rawValue:
            self.firstTeam.clearInputText()
            self.secondTeam.clearInputText()
        case SpecificAction.tichuATeam.rawValue:
            if (self.firstTeam.inputNumber <= 225 || self.firstTeam.inputNumber == 300) && self.firstTeam.inputNumber % 5 == 0 && self.secondTeam.inputNumber % 5 == 0 {
                self.firstTeam.updateInputForOneHundred(isMinus: false)
                self.completeWithZero(team: self.secondTeam)
            }
        case SpecificAction.minusTichuATeam.rawValue:
            if (self.firstTeam.inputNumber >= -125 || self.firstTeam.inputNumber == -300 || self.firstTeam.inputNumber == -200) && self.firstTeam.inputNumber % 5 == 0 && self.secondTeam.inputNumber % 5 == 0 {
                self.firstTeam.updateInputForOneHundred(isMinus: true)
                self.completeWithZero(team: self.secondTeam)
            }
        case SpecificAction.tichuBTeam.rawValue:
            if (self.secondTeam.inputNumber <= 225 || self.secondTeam.inputNumber == 300) && self.firstTeam.inputNumber % 5 == 0 && self.secondTeam.inputNumber % 5 == 0 {
                self.secondTeam.updateInputForOneHundred(isMinus: false)
                self.completeWithZero(team: self.firstTeam)
            }
        case SpecificAction.minusTichuBTeam.rawValue:
            if (self.secondTeam.inputNumber >= -125 || self.secondTeam.inputNumber == -300 || self.secondTeam.inputNumber == -200) && self.firstTeam.inputNumber % 5 == 0 && self.secondTeam.inputNumber % 5 == 0 {
                self.secondTeam.updateInputForOneHundred(isMinus: true)
                self.completeWithZero(team: self.firstTeam)
            }
        default:
            if currentTeam!.inputDigits.count < 2 {
                if currentTeam!.inputDigits.count == 1 {
                    if action % 5 != 0 {
                        return
                    }
                }
                currentTeam?.updateInputText(inputDigit: action)
                self.autoCompletion(team: currentTeam!)
            }
        }
    }
    
    func inputForTeamWith(selection: Int) -> TichuTeam? {
        if selection == 1 {
            return self.firstTeam
        }
        else if selection == 2 {
            return self.secondTeam
        }
        else {
            print("error")
            return nil
        }
    }
    
    func completeWithZero(team: TichuTeam) {
        if team.inputText == "" {
            team.inputNumber = 0
            team.inputDigits.append(0)
            team.inputText = String(team.inputNumber)
        }
    }
    
    func autoCompletion(team: TichuTeam) {
        if team === self.firstTeam {
            var tmpNumber = 0
            for (index,digit) in self.firstTeam.inputDigits.reversed().enumerated() {
                tmpNumber += Utils.power(base: 10, power: index) * digit
            }
            if tmpNumber % 5 != 0 {
                return
            }
            if tmpNumber == 100 || tmpNumber == 200 {
                self.secondTeam.inputNumber = 0
            }
            else {
                self.secondTeam.inputNumber = DataSettingsController.TOTAL_SCORE - tmpNumber
            }
            self.secondTeam.inputDigits.append(self.secondTeam.inputNumber / 10)
            self.secondTeam.inputDigits.append(self.secondTeam.inputNumber % 10)
            self.secondTeam.inputText = String(self.secondTeam.inputNumber)
        }
        else if team === self.secondTeam {
            var tmpNumber = 0
            for (index,digit) in self.secondTeam.inputDigits.reversed().enumerated() {
                tmpNumber += Utils.power(base: 10, power: index) * digit
            }
            if tmpNumber % 5 != 0 {
                return
            }
            if tmpNumber == 100 || tmpNumber == 200 {
                self.firstTeam.inputNumber = 0
            }
            else {
                self.firstTeam.inputNumber = DataSettingsController.TOTAL_SCORE - tmpNumber
            }
            self.firstTeam.inputDigits.append(self.firstTeam.inputNumber / 10)
            self.firstTeam.inputDigits.append(self.firstTeam.inputNumber % 10)
            self.firstTeam.inputText = String(self.firstTeam.inputNumber)
        }
    }
    
    func resetAllInputs() {
        self.firstTeam.clearInputText()
        self.secondTeam.clearInputText()
    }
    
    func setLastModifiedDate() {
        self.modifiedDate = Date()
    }
    
}
