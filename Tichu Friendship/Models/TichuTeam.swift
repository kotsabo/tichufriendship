//
//  TichuTeam.swift
//  Tichu Friendship
//
//  Created by Ilias Kotsampougioukoglou on 07/06/2017.
//  Copyright © 2017 Ilias Kotsampougioukoglou. All rights reserved.
//

import Foundation

class TichuTeam: NSObject, NSCoding {
    
    var name: String = ""
    var totalScore: Int = 0
    var roundsScore: [Int] = []
    
    var inputText: String = ""
    var inputNumber: Int = 0
    var inputDigits: [Int] = []
    
    override init() {
        
    }
    
    convenience required init(coder aDecoder: NSCoder) {
        self.init()
        self.name = aDecoder.decodeObject(forKey: "name") as! String
        self.totalScore = aDecoder.decodeInteger(forKey: "totalScore")
        self.roundsScore = aDecoder.decodeObject(forKey: "roundsScore") as! [Int]
        self.inputText = aDecoder.decodeObject(forKey: "inputText") as! String
        self.inputNumber = aDecoder.decodeInteger(forKey: "inputNumber")
        self.inputDigits = aDecoder.decodeObject(forKey: "inputDigits") as! [Int]
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(totalScore, forKey: "totalScore")
        aCoder.encode(roundsScore, forKey: "roundsScore")
        aCoder.encode(inputText, forKey: "inputText")
        aCoder.encode(inputNumber, forKey: "inputNumber")
        aCoder.encode(inputDigits, forKey: "inputDigits")
    }
    
    func updateScore(score: Int) {
        self.roundsScore.append(score)
        self.totalScore += score
    }
    
    func deleteLastRound() {
        if self.roundsScore.count > 0 {
            self.totalScore -= self.roundsScore.last!
            self.roundsScore.removeLast()
        }
    }
    
    func updateInputText(inputDigit: Int) {
        self.inputDigits.append(inputDigit)
        self.inputNumber = 0
        for (index,digit) in self.inputDigits.reversed().enumerated() {
            self.inputNumber += Utils.power(base: 10, power: index) * digit
        }
        self.inputText = String(self.inputNumber)
    }
    
    func updateInputForOneHundred(isMinus: Bool) {
        self.constructInputDigitsForOneHundred()
        if isMinus {
            self.inputNumber -= 100
        }
        else {
            self.inputNumber += 100
        }
        self.inputText = String(self.inputNumber)
    }
    
    func constructInputDigitsForOneHundred() {
        if self.inputDigits.count == 0 {
            self.inputDigits.append(1)
            self.inputDigits.append(0)
            self.inputDigits.append(0)
        }
        else if self.inputDigits.count == 1 {
            self.inputDigits.insert(0, at: 0)
            self.inputDigits.insert(1, at: 0)
        }
        else if self.inputDigits.count == 2 {
            self.inputDigits.insert(1, at: 0)
        }
        else if self.inputDigits.count == 3 {
            self.inputDigits[0] += 1
        }
        else {
            print("sth is going totally wrong")
        }
    }
    
    func clearInputText() {
        self.inputDigits = []
        self.inputNumber = 0
        self.inputText = ""
    }
    
    func resetAll() {
        self.name = ""
        self.totalScore = 0
        self.roundsScore = []
        self.inputText = ""
        self.inputNumber = 0
        self.inputDigits = []
    }
    
    
}
